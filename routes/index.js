var express = require('express');
var router = express.Router();
var Web3 = require('web3');
const Tx = require('ethereumjs-tx');
var Request = require("request");
var web3 = require('web3-utils');
var sha256 = require('sha256');
const abi=[
	{
		"constant": false,
		"inputs": [
			{
				"name": "_refPerscription",
				"type": "uint256"
			},
			{
				"name": "_refPatient",
				"type": "uint256"
			},
			{
				"name": "_refDoctor",
				"type": "uint256"
			},
			{
				"name": "_medicaments",
				"type": "uint256[]"
			},
			{
				"name": "_infoHashs",
				"type": "bytes32[]"
			}
		],
		"name": "addPerscriptions",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_id",
				"type": "uint256"
			}
		],
		"name": "getPerscription",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "uint256[]"
			},
			{
				"name": "",
				"type": "bytes32[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];


/* GET home page. */
router.get('/addPerscription', function(req, res, next) {
  var hash = sha256.x2('data');
  console.log(hash);
  web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/5b84fe1bb08842b7bfcba7f05e8f87db"));
  var number = web3.eth.getTransactionCount("0xA1C8BBb1B86aDC6f3C74743E2e5dD8d12296F0DA", "pending");
  var data = web3.eth.contract(abi).at("0x44a108f847f83fad39574a246bb12655f686264d");
  var add = data.addPerscriptions.getData(1,2,3,[1,2],[hash,hash]);
  var privateKey = '56FF8D1EACB8E33E034090DEC1D11F2531DEE762070036ACDEB7719317ADB7D8';

    var tx = new Tx({
      nonce: number,
      gasPrice: web3.toHex(web3.toWei('1000', 'gwei')),
      gasLimit: 1000000,
      to: "0x44a108f847f83fad39574a246bb12655f686264d",
      value: 0,
      data: add,
	});
	tx.sign(Buffer(privateKey, 'hex'));


	var raw = '0x' + tx.serialize().toString('hex');
    web3.eth.sendRawTransaction(raw, function (err, transactionHash) {
		console.log(transactionHash);
		res.json(transactionHash);
	});
	
	
	
});

/* GET home page. */
router.get('/perscriptionDetails', function(req, res, next) {
	var hash = sha256.x2('data');
	console.log(hash);
	web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/5b84fe1bb08842b7bfcba7f05e8f87db"));
	var number = web3.eth.getTransactionCount("0xA1C8BBb1B86aDC6f3C74743E2e5dD8d12296F0DA", "pending");
	var data = web3.eth.contract(abi).at("0x44a108f847f83fad39574a246bb12655f686264d");
	res.json(data.getPerscription(1));

  });


module.exports = router;
