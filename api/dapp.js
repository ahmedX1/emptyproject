var express = require('express');
var router = express.Router();
const Web3 = require('web3');
var contractAddress = '3d49d340f92b7cd0b6d3efca344f2dec6ce360ec';
var provider = "http://192.168.247.129:5000";

var myContract;

function getContract() {
    
}

router.get('/',function (req, res, next) {
    console.log("Getting the Contract")

    var web3 = new Web3();
    web3.setProvider(new web3.providers.HttpProvider(provider));
    web3.eth.defaultAccount = web3.eth.accounts[0];

    var address = contractAddress;

    console.log("Got address: " + address)

    var loyaltyABI = [
        {
            "constant": true,
            "inputs": [],
            "name": "hello",
            "outputs": [
                {
                    "name": "",
                    "type": "string"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "name",
                    "type": "string"
                }
            ],
            "name": "set",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "yourName",
            "outputs": [
                {
                    "name": "",
                    "type": "string"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "inputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "constructor"
        }
    ]
    var LoyaltyContract = web3.eth.contract(loyaltyABI);
    myContract = LoyaltyContract.at(address);
    console.log(myContract.hello());
});
  

router.get('/getname', function(req, res, next) {
    
});

module.exports = router;
