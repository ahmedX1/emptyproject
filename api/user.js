var express = require('express');
var router = express.Router();
var Web3 = require('web3');
const Tx = require('ethereumjs-tx');
var Request = require("request");
var sha256 = require('sha256');
const knex  = require('../db/connetion');
const static = require('../static/static');

router.post('/addUsers/:ref',function (req, res, next) {
    const ref = parseInt(req.params.ref);
    const userAddress = req.body.userAddress;
    web3 = new Web3(new Web3.providers.HttpProvider(static.providerUrl));
    var number = web3.eth.getTransactionCount("0xa1c8bbb1b86adc6f3c74743e2e5dd8d12296f0da", "pending");
    var data = web3.eth.contract(static.abiAccess).at(static.scAccess);
    var addUser = data.addUser.getData(userAddress,ref);
    var privateKey = '56FF8D1EACB8E33E034090DEC1D11F2531DEE762070036ACDEB7719317ADB7D8';
    
      var tx = new Tx({
        nonce: number,
        gasPrice: web3.toHex(web3.toWei('100', 'gwei')),
        gasLimit: 1000000,
        to: static.scAccess,
        value: 0,
        data: addUser,
      });
      
    tx.sign(Buffer(privateKey, 'hex'));
    var raw = '0x' + tx.serialize().toString('hex');
      web3.eth.sendRawTransaction(raw, function (err, transactionHash) {
      if(!err){
        return res.status(200)
        .json({'success' : 1 ,'message' : 'gg!','data':transactionHash})
        .end();
      }else{
        return res.status(200)
        .json({'success' : 0 ,'message' : 'failed to send transaction!'})
        .end();
      }
    });
  })
  

router.get('/role/:address', function(req, res, next) {
    address = req.params.address;
    web3 = new Web3(new Web3.providers.HttpProvider(static.providerUrl));
    var data = web3.eth.contract(static.abiAccess).at(static.scAccess);
    res.json(data.getUser(address));
});

router.get('/transactionStatus/:hash', function (req, res) {
    var hash = req.params.hash;
    var web3 = new Web3(new Web3.providers.HttpProvider(static.providerUrl));
    web3.eth.getTransactionReceipt(hash, function (err, data) {
        if (!err)
            res.send(data);
        else
            res.send(err);
    })
});


router.get('/test/:hash', function (req, res) {
  var hash = req.params.hash;
  var web3 = new Web3(new Web3.providers.HttpProvider(static.providerUrl));
  web3.eth.getTransactionReceipt(hash, function (err, data) {
      if (!err)
          res.send(data);
      else
          res.send(err);
  })
});

router.post('/addDoctor', function (req, res) {
  const doctor = {
    address : req.body.address,
    pwd : req.body.pwd,
    specialty : req.body.specialty,
    name : req.body.name
  }
  knex('doctors')
  .insert(doctor)
  .then(()=>{
    return res.status(200)
    .json({'success' : 1 ,'message' : 'Doctor added successfully !'})
    .end();
  },()=>{
    return res.status(200)
    .json({'success' : 0 ,'message' : 'unable to add !'})
    .end();
  });
});

router.post('/login/:id', function(req, res, next) {
      const id = req.params.id;
      const address = req.body.address;
      const pwd = req.body.pwd;
      if(id == 3 ){
        knex('doctors')
        .where({'address': address,'pwd':pwd})
        .then((data) => {
          if(data.length !== 0){
            return res.status(200)
            .json({ 'success': true, 'message': 'login success!','data':data[0]})
            .end();
          }else{
            return res.status(200)
            .json({ 'success': 0, 'message': 'login Failed!'})
            .end();
          }
        }, (err) => {
          return res.status(200)
            .json({ 'success': false, 'message': 'Data base error !' + err })
            .end();
        });
      }
      if(id == 2 ){
        
      }
});
module.exports = router;
